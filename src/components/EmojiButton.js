import React, { Component } from 'react';
import { connect } from 'react-redux';
import EmojiPicker from './EmojiPicker';
import { Button } from 'reactstrap';

class EmojiButton extends Component {
    render() {
        return (
            <div className="emoji-button">
                <EmojiPicker>
                    <Button type="button">
                        EMOJIS!
                    </Button>
                </EmojiPicker>
            </div>
        )
    }
}

export default connect()(EmojiButton);
