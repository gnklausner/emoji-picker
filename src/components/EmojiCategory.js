import React from 'react';
import unicodeMap from 'emoji-unicode-map';
import { emojis, categories } from './emojis';
import { startCase } from 'lodash';

const EmojiCategory = (props) => {
  let getEmojiString = (emoji) => { return unicodeMap.get(emoji) || '' }
  let refinedEmojis = emojis[props.category].filter(emoji => {
    return getEmojiString(emoji).includes(props.searchString)
  });

  const emojiList = refinedEmojis.map((emoji) => {
      return (
        <div className='emoji'
             onClick={() => props.handleClick(emoji)}
             key={unicodeMap.get(emoji) || emoji}>
          {emoji}
        </div>
      )
  });

  let display = refinedEmojis.length !== 0;

  return(
    <div id={props.category} className={display ? '' : 'hidden' }>
      <div>{startCase(props.category)} {categories[props.category]}</div>
      <div className='emoji-list'>
          {emojiList}
      </div>
    </div>
  )
};

export default EmojiCategory;
