import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Popover, PopoverHeader, PopoverBody } from 'reactstrap';
import { categories } from './emojis';
import { setEmoji } from '../redux/actions';
import { goToAnchor } from 'react-scrollable-anchor';
import EmojiCategory from './EmojiCategory';

class EmojiPicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            searchString: '',
        };
    }

    handleChange = (event) => {
        this.setState({ searchString: event.target.value });
    }

    buildEmojiCategoriesAnchors = () => {
        const emojiCategories = [];
        const categoryGotos = [];
        Object.keys(categories).forEach(category => {
            emojiCategories.push(
                <EmojiCategory category={category}
                               key={category}
                               handleClick={this.props.setEmoji}
                               searchString={this.state.searchString} />
            );

            categoryGotos.push(
                <span onClick={() => goToAnchor(category, false)} className='emoji'>{categories[category]}</span>
            );
        });

        return [emojiCategories, categoryGotos];
    }

    render() {
        const { isOpen } = this.state;

        let emojiCategories, categoryGotos;
        [emojiCategories, categoryGotos] = this.buildEmojiCategoriesAnchors();

        return (
            <div>
                <div id="EmojiPopover">
                    {this.props.children}
                </div>
                <Popover placement="bottom"
                         isOpen={this.state.isOpen}
                         target="EmojiPopover"
                         toggle={() => this.setState({ isOpen: !isOpen })}>
                    <PopoverHeader>
                        <div className='category-goto'>{categoryGotos}</div>
                        <div>
                            <input type='text' placeholder='search' className='emoji-search' onChange={this.handleChange} />
                        </div>
                    </PopoverHeader>
                    <PopoverBody>
                        <div className='emojis-container'>{emojiCategories}</div>
                    </PopoverBody>
                </Popover>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return({
        setEmoji: (emoji) => {dispatch(setEmoji(emoji))}
    })
}

export default connect(null, mapDispatchToProps)(EmojiPicker);
