# Summary

## Troubles and Trade-Offs:

Revisiting `Redux` for the first time in a while was easily the most challenging aspect of this project. I needed to refresh on `Redux` basics, specifically around matching dispatchers to a component's props through the `connect` API. Expanding upon this by learning about `Thunk` also took some precious time. All of this reading felt like it was taking precious time away from developing, which further exacerbated the tension between wanting to write good code and getting features out the door. Ultimately, I concluded to focus on writing "okay" code that finishes a feature, with the knowledge that I can refactor later when necessary. There are numerous opportunities to improve the `EmojiPicker` component: `EmojiPicker` has too many responsibilities, the styles should be moved to a separate stylesheet, and the performance is no longer acceptable.

The most obvious tradeoff that occurred is the ease of development versus performance costs when switching from `react-tether` to `reactstrap`. The loading of the popover was noticibly slower when using `reactstrap`. However, styling `react-tether` is more difficult and the API for configuration is not as simple. I would require more time to properly learn how to use the options for `react-tether`. [Switching to a canvas element](https://www.nylas.com/blog/the-developers-guide-to-emoji/) seems to be the way to go for performant emoji pickers.


### Time Worked: ~2 hours reading, ~1.5 hours coding
